package android.example.com.listadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView list = (ListView) findViewById(R.id.listView);


        Person jacky = new Person("Jacky", "2019-03-25", "Male");
        Person jean = new Person("Jean", "2019-03-25", "Female");
        Person thomas = new Person("Thomas", "2019-03-25", "Male");
        Person daphne = new Person("Daphne", "2019-03-25", "Female");
        Person suki = new Person("Suki", "2019-03-25", "Female");
        Person elycia = new Person("Elycia", "2019-03-25", "Female");
        Person abhi = new Person("Abhijeet", "2019-03-25", "Female");
        Person tylor = new Person("Tylor", "2019-03-25", "Male");
        Person cole = new Person("Cole", "2019-03-25", "Male");
        Person dave = new Person("Dave", "2019-03-25", "Male");
        Person ian = new Person("Ian", "2019-03-25", "Male");

        ArrayList<Person> people = new ArrayList<>();
        people.add(jacky);
        people.add(jean);
        people.add(thomas);
        people.add(daphne);
        people.add(suki);
        people.add(elycia);
        people.add(abhi);
        people.add(tylor);
        people.add(cole);
        people.add(dave);
        people.add(ian);

        PersonListAdapter adapter = new PersonListAdapter(this, R.layout.adapter_view_layout, people);
        list.setAdapter(adapter);

    }
}
